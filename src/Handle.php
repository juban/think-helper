<?php

namespace think\admin;

use think\exception\HttpResponseException;
use think\facade\Log;
use think\Response;
use Throwable;

class Handle extends \think\exception\Handle
{
    public function report(Throwable $exception): void
    {
        if ($exception instanceof HttpResponseException) {
            Log::info($exception->getResponse()->getData());
        } else {
            $msg = $exception->getMessage();
            $info = $exception->getTraceAsString();

            Log::info("$msg:\n$info");
        }
    }

    public function render($request, Throwable $e): Response
    {
        if ($e instanceof HttpResponseException) {
            return $e->getResponse();
        }

        return parent::render($request, $e);
    }
}